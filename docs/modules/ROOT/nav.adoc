* xref:features.adoc[How Antora Can Help]
* xref:how-antora-works.adoc[How Antora Works]

* Get Antora
** xref:supported-platforms.adoc[System Requirements]
*** xref:install/linux-requirements.adoc[Linux]
*** xref:install/macos-requirements.adoc[macOS]
*** xref:install/windows-requirements.adoc[Windows]

** xref:install/install-antora.adoc[Install Antora]
** xref:install/upgrade-antora.adoc[Upgrade Antora]

* xref:run-antora.adoc[Run Antora]
** xref:run-antora.adoc#local-site-preview[Local Preview]
** xref:run-antora.adoc#cache[Cache]
** xref:antora-container.adoc[Run Antora in a Container]
** xref:cli:index.adoc[CLI Reference]

* xref:component-structure.adoc[File Organization]
** xref:modules.adoc[Modules]
//** Pages & Partials
//** Assets
//** Examples
** xref:component-descriptor.adoc[Component Descriptor]
** xref:component-versions.adoc[Versions & Branches]

//* Source Files
//** Content and asset files
//** Navigation files
//** UI files
//** Documentation component
//
//.Configure
//* Playbook files
//
//.Publishing
